const http = require ('http');

const port = 5000;

//Mock database
let directory = [
			{
	                "firstName": "Mary Jane",
	                "lastName": "Dela Cruz",
	                "mobileNo": "09123456789",
	                "email": "mjdelacruz@mail.com",
	                "password": 123
	        },
	        {
	                "firstName": "John",
	                "lastName": "Doe",
	                "mobileNo": "09123456789",
	                "email": "jdoe@mail.com",
	                "password": 123
	        }

]

console.log(typeof directory)

const server = http.createServer((req, res) => {

	if (req.url == '/profile' && req.method == "GET"){
		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.write(JSON.stringify(directory));
		res.end()
	}


//create users (POST METHOD)
	if(req.url == '/profile' && req.method == "POST"){
		
		let requestBody = '';

		req.on('data', function(data){
			requestBody += data;

		});


		req.on('end', function() {
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody)


			let newInputs = {
				 	"firstName": requestBody.firstName,
	                "lastName": requestBody.lastName,
	                "mobileNo": requestBody.mobileNo,
	                "email": requestBody.email,
	                "password": requestBody.password

			}

			directory.push(newInputs);
			console.log(directory)

			res.writeHead(200, {'Content-Type':'application/json'});
			res.write(JSON.stringify(newInputs));
			res.end()
		})


	}


});

server.listen(port);

console.log(`Server running at localhost: ${port}`);

